import Vue from 'vue'
import Router from 'vue-router'
import HomePage from '@/pages/HomePage'
import TestPage from '@/pages/TestPage'
import VeeValidate from 'vee-validate'

Vue.use(VeeValidate)
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomePage
    },
    {
      path: '/home',
      regirect: { name: 'home' }
    },
    {
      path: '/cases',
      name: 'cases',
      component: TestPage
    },
    {
      path: '/contact',
      name: 'contract',
      component: TestPage
    }
  ]
})
